"Tema, justificativa, objetivos, metodologia, resultados parciais e/ou finais e considerações finais."

Ferramentas que visam facilitar a publicação de conteúdo na internet se tornaram a porta de entrada para a maioria das pessoas que publicam sites pessoais e blogs. O Wordpress é uma destas ferramentas e hoje é utilizado em cerca de 70% dos sites deste tipo. A sua última versão estável, lançada em agosto de 2015 já contabiliza mais de 17 milhões de downloads.

Os desenvolvedores disponibilizam periodicamente uma série de correções de segurança entretanto, a utilização de plugins e temas de terceiros acaba por comprometer toda a aplicação e constantemente a tornam vulneráveis.










Ferramentas que visam facilitar a publicação de conteúdo na internet se tornaram a porta de entrada para a maioria das pessoas que publicam sites pessoais e blogs. Um exemplo disso é o Wordpress, CMS (Content Management System) surgido em 2003, utilizado em quase 70% das aplicações deste tipo e que permite que um site seja publicado com pouco ou nenhum conhecimento técnico. Uma das facilidades que a ferramenta possui é o fato de ter uma enorme quantidade de plug-ins e temas disponibilizados gratuitamente na internet, desenvolvidos por pessoas no mundo todo. Estes pacotes de terceiros são capazes de adicionar funcionalidades a um site ou mudar o visual dele em questão de minutos, por isso são tão populares e utilizados. Visando garantir a segurança dos dados e das aplicações que usam o Wordpress como base, a empresa responsável pelo desenvolvimento da plataforma publica regularmente correções de erros e falhas de segurança. O mesmo não acontece com as aplicações de terceiros (plug-ins e temas), que não passam por homologação junto a nenhuma empresa e em sua grande maioria não possuem testes de segurança. A falta deste processo, aliada ao pouco conhecimento técnico do usuário, pode comprometer toda a aplicação, uma vez que seja instalado um plug-in com alguma falha de segurança. Dentre as vulnerabilidades mais comuns exploradas estão o XSS, que consiste na execução de um script malicioso dentro do site alvo, e o CSRF, que consiste em se aproveitar das sessões do navegador para acessar uma aplicação e extrair dados da mesma. Ambas estão no TOP 10 da OWASP (Open Web Application Security Project), comunidade dedicada à segurança de aplicações web, e sua utilização em uma aplicação Wordpress vulnerável pode fazer com que o atacante consiga acesso de administrador sem que o verdadeiro administrador perceba. A partir dessas constatações, este trabalho visa discutir e analisar diversos aspectos relevantes à segurança sobre estes ataques, objetivando inicialmente: estudo comparativo da segurança da plataforma com ou sem os plug-ins, baseado nas vulnerabilidades, estudo quantitativo de coleta das informações sensíveis das aplicações, análise e levantamento de vetores de ataque a estas vulnerabilidades, desenvolvimento e implementação de uma ferramenta para identificação e mitigação de tais ataques.

Palavras-chave: XSS. CSRF. Wordpress. Segurança.